from django.urls import path
from django.contrib.auth import views
from receipts.views import (
    AccountCreateView,
    CategoryCreateView,
    ReceiptListView,
    ReceiptCreateView,
    CategoryListView,
    AccountListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="new_receipt"),
    path("accounts/create/", AccountCreateView.as_view(), name="new_account"),
    path(
        "categories/create/", CategoryCreateView.as_view(), name="new_category"
    ),
    path("categories/", CategoryListView.as_view(), name="list_category"),
    path("accounts/", AccountListView.as_view(), name="list_account"),
]
